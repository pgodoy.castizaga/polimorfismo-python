from Formas import FormaGeometrica

class Rectangulo(FormaGeometrica):

    def __init__(self, ancho, alto):
        self.ancho = ancho
        self.alto = alto

    def calcular_area(self):
        return self.ancho * self.alto