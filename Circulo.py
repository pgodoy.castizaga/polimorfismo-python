from Formas import FormaGeometrica

class Circulo(FormaGeometrica):

    def __init__(self, radio):
        self.radio = radio

    def calcular_area(self):
        import math
        return math.pi * self.radio ** 2