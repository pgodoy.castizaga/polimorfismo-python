class CuentaBancaria:
    
    def __init__(self, titular, saldo_inicial):
        self.titular = titular
        self.saldo = saldo_inicial

    def depositar(self, cantidad):
        if cantidad > 0:
            self.saldo += cantidad
            print(f"Deposito de ${cantidad} realizado. Saldo actual: ${self.saldo}")
        else:
            print("La cantidad a depositar debe ser mayor a cero.")

    def retirar(self, cantidad):
        if cantidad > 0 and cantidad <= self.saldo:
            self.saldo -= cantidad
            print(f"Retiro de ${cantidad} realizado. Saldo actual: ${self.saldo}")
        else:
            print("La cantidad a retirar debe ser mayor a cero y no puede superar su saldo.")

    def consultar_saldo(self):
        print(f"Saldo actual de {self.titular}: ${self.saldo}")